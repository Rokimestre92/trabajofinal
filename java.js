function rad_deg(rad){
  return (rad*360/(2*Math.PI));
}

function deg_rad(deg){
  return ((2*Math.PI*deg)/360);
}

function calc_2(aC,lb,la){
  var la;
  var aAr;
  var aB;
  var aC;
  
aA = parseFloat(aC);
lb=parseFloat(lb);
lc=parseFloat(la);

  
if(isNaN(aC) || aC<=0 || aC>=180){
    document.resolv2.txtres.value= " ¡ Error en el Angulo Y° !";
    return;
  }
  
if(isNaN(lb) || lb<=0){
    document.resolv2.txtres.value = " ¡ Error en el Lado b !";
    return;
  }
  
if(isNaN(la) || la<=0){
    document.resolv2.txtres.value= " ¡ Error en el Lado a !";
    return;
  }
 
aCr =deg_rad(aC); 
lc=Math.sqrt(lb*lb+la*la-2*lb*la*Math.cos(aCr));

  
if(lb<=la){
aB= rad_deg(Math.asin(Math.sin(aCr)*lb/lc));
aB =Math.round(aB * Math.pow(10, 3)) / Math.pow(10,3);
 aA=180-aC-aB;
 aA= Math.round(aA * Math.pow(10, 3)) / Math.pow(10,3);
  }

else{
    aA= rad_deg(Math.asin(Math.sin(aCr)*la/lc));
    aA =Math.round(aA * Math.pow(10, 3)) / Math.pow(10,3);
    aB=180-aC-aA;
    aB= Math.round(aB * Math.pow(10, 3)) / Math.pow(10,3);  
  }
  
document.resolv2.l_c.value= Math.round(lc * Math.pow(10, 3)) / Math.pow(10,3);
document.resolv2.a_B.value = aB+"º";
document.resolv2.a_A.value = aA+"º";
document.resolv2.txtres.value = " ¡ RESUELTO !";

}

function borr_r2(){
  document.resolv2.l_c.value= "";
  document.resolv2.a_B.value = "";
  document.resolv2.a_A.value = "";
  document.resolv2.txtres.value = "Introduce los datos";
}






function calc_3(aB,la,aC){
  var lb;
  var lc;
  var aA;
  var aAr;
  var aBr;
  var aCr;

  
  
la =parseFloat(la);
aB= parseFloat(aB);
aC= parseFloat(aC);

	
if(isNaN(aB) || aB<=0 || aB>=180){
    document.resolv3.txtres.value= " ¡ Error en el ¡Angulo B !";
    return;
  }

 
if(isNaN(la) || la<=0){
    document.resolv3.txtres.value = " ¡ Error en el Lado a !";
    return;
  }
  
	
if(isNaN(aC) || aC<=0 || aC>=180){
    document.resolv3.txtres.value= " ¡ Error en el ¡Angulo Y !";
    return;
  }
  
	
if((aB+aC)>=180){
    document.resolv3.txtres.value = " ¡ A + B > 180º !";
    return;
  }

  
aBr = deg_rad(aB);
aCr = deg_rad(aC);
 
aA=180-aC-aB;
aAr = deg_rad(aA);
aA= Math.round(aA * Math.pow(10, 3)) / Math.pow(10,3);
  	
lb= la*Math.sin(aBr)/Math.sin(aAr);
lc= la*Math.sin(aCr)/Math.sin(aAr);
    
  
document.resolv3.l_b.value = Math.round(lb * Math.pow(10, 3)) / Math.pow(10,3);
document.resolv3.l_c.value = Math.round(lc * Math.pow(10, 3)) / Math.pow(10,3);
document.resolv3.a_A.value = aA+"º";
document.resolv3.txtres.value = " ¡ RESUELTO !";

} 

function borr_r3(){
  document.resolv3.l_b.value = "";
  document.resolv3.l_c.value = "";
  document.resolv3.a_A.value = "";
  document.resolv3.txtres.value = "Introduce los datos";
}